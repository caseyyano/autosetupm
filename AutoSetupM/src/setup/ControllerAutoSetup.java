package setup;

import java.util.HashMap;
import java.util.Map;

public class ControllerAutoSetup {
	private static Map<String, ControllerButton> buttonMap = new HashMap<String, ControllerButton>();

	public static void initializeControllerAutoSetup() {
		buttonMap.put("L", new ControllerButton(3, 4));
		buttonMap.put("R", new ControllerButton(3, 4));
		buttonMap.put("DU", new ControllerButton(5, 2));
		buttonMap.put("DS", new ControllerButton(6, 1));
		buttonMap.put("DD", new ControllerButton(7, 0));
		buttonMap.put("Z", new ControllerButton(3, 4));
		buttonMap.put("X", new ControllerButton(2, 5));
		buttonMap.put("Y", new ControllerButton(2, 5));
		buttonMap.put("A", new ControllerButton(0, 7));
		buttonMap.put("B", new ControllerButton(1, 6));
		buttonMap.put("C", new ControllerButton(5, 0));
	}

	/**
	 * Setup for controller from the input name screen
	 * 
	 * @throws InterruptedException
	 */
	public static void setupController(boolean tapJump, String input)
			throws InterruptedException {

		// If default settings, just return to input name screen
		if (tapJump && input == null) {
			AutoSetupM.type("B", 1300);
			return;
		} else {
			AutoSetupM.type("A", 475);
		}

		// Turn off Tap Jump
		if (!tapJump) {
			AutoSetupM.type("LEFT", 150);
			AutoSetupM.type("A", 200);
			if (input == null) {
				AutoSetupM.type("UP", 150);
				AutoSetupM.type("A", 200);

				// Close "Configuration Saved" pop-up
				AutoSetupM.type("A", 140);

				// Leaves controller select (returns to input name screen)
				AutoSetupM.type("B", 1950);
				return;
			} else {
				AutoSetupM.type("RIGHT", 150);
			}
		}

		String[] inputSettings = input.split(" ");

		// L Button
		buttonMapHelper("L", inputSettings);
		AutoSetupM.type("DOWN");

		// D-Pad Up
		buttonMapHelper("DU", inputSettings);
		AutoSetupM.type("DOWN");

		// D-Pad Side
		buttonMapHelper("DS", inputSettings);
		AutoSetupM.type("DOWN");

		// D-Pad Down
		buttonMapHelper("DD", inputSettings);
		AutoSetupM.type("RIGHT");

		// C-Stick
		buttonMapHelper("C", inputSettings);
		AutoSetupM.type("UP");

		// B Button
		buttonMapHelper("B", inputSettings);
		AutoSetupM.type("UP");

		// A Button
		buttonMapHelper("A", inputSettings);
		AutoSetupM.type("UP");

		// X Button
		buttonMapHelper("X", inputSettings);
		AutoSetupM.type("UP");

		// Y Button
		buttonMapHelper("Y", inputSettings);
		AutoSetupM.type("UP");

		// Z Button
		buttonMapHelper("Z", inputSettings);
		AutoSetupM.type("UP");

		// R Button
		buttonMapHelper("R", inputSettings);
		AutoSetupM.type("UP");
		AutoSetupM.type("RIGHT");
		AutoSetupM.type("A", 200);

		// Close "Configuration Saved" pop-up
		AutoSetupM.type("A", 140);

		// Leaves controller select (returns to input name screen)
		AutoSetupM.type("B", 1950);
	}

	public static void buttonMapHelper(String key, String[] inputSettings)
			throws InterruptedException {
		for (String s : inputSettings) {
			if (s.contains(key)) {

				// Error handling for improper values (too much/little text)
				if (s.length() == key.length()
						|| (s.length() > key.length() + 2)) {
					System.out.println("[ERROR]: Improper value specified for "
							+ key + ". " + s);
					return;
				}

				int i = Integer
						.parseInt(Character.toString(s.charAt(s.length() - 1)));
				boolean success = false;

				// Handle negative and positive. Works with no '+' sign as well
				if (s.charAt(s.length() - 2) == '-') {
					if (i <= buttonMap.get(key).lowerLimit) {
						AutoSetupM.type("A");
						for (int j = 0; j < i; j++) {
							AutoSetupM.type("LEFT");
						}
						success = true;
					} else {
						System.out
								.println("[ERROR]: Value is out of range for "
										+ key + ". At -" + i);
					}
				} else {
					if (i <= buttonMap.get(key).upperLimit) {
						AutoSetupM.type("A");
						for (int j = 0; j < i; j++) {
							AutoSetupM.type("RIGHT");
						}
						success = true;
					} else {
						System.out
								.println("[ERROR]: Value is out of range for "
										+ key + ". At " + i);
					}
				}

				if (success) {
					AutoSetupM.type("A");
				}
				break;
			}
		}
	}
}

package setup;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.ini4j.Ini;

/**
 * This was supposed to be a tiny script (driver only). It got a bit bigger.
 * It's my excuse for the disorganization
 * 
 * @param args
 * @throws AWTException
 * @throws InterruptedException
 */
public class AutoSetupM {

	private static Robot r;
	private static boolean logging, isWidescreen, isMuted,
			disableAutoSetupStages, fromMainMenu;
	private static String choiceMode, destination;
	private static Map<String, Integer> keyMap = new HashMap<String, Integer>();
	private static long latency;
	private static long keyEventDuration = 35;
	public static long minLatency = 25;

	/**
	 * Generic driver logic is here. Workflow is visible here
	 * 
	 * @param args
	 * @throws AWTException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void main(String[] args) throws AWTException,
			InterruptedException, IOException {

		// Allows execution from the Main Menu
		if (args.length > 0) {
			if (args[0].equals("mainmenu")) {
				fromMainMenu = true;
			}
		}

		long startTime = System.currentTimeMillis();

		initialize();
		navigateToMainMenu();
		setupWidescreen();
		setupSound();
		RulesAutoSetup.setupItems();
		RulesAutoSetup.setupStageChoice(choiceMode);
		RulesAutoSetup.setupStages(disableAutoSetupStages);
		ProfileAutoSetup.createProfiles();
		goToDestination();

		System.out.println("[INFO]: Setup completed in "
				+ (System.currentTimeMillis() - startTime) / 1000
				+ " seconds. \n\nDyno says, \"Good luck and have fun!\"");
		System.out
				.println("\nThis window will close automatically in 5 seconds");
		Thread.sleep(5000);
	}

	private static void goToDestination() throws InterruptedException {
		if (destination.equals("FIGHT")) {
			type(new String[] { "B", "B", "UP", "LEFT", "A", "A" });
		} else if (destination.equals("NAMEENTRY")) {
			// Do nothing
		}
	}

	/**
	 * Initializes Robot and reads settings.ini for settings, profiles, etc
	 * 
	 * @throws AWTException
	 * @throws IOException
	 */
	public static void initialize() throws AWTException, IOException {
		r = new Robot();

		// Initialize general settings
		Ini ini = new Ini(new File("config/settings.ini"));
		logging = Boolean.valueOf(ini.get("general", "logging"));
		latency = Long.parseLong(ini.get("general", "latency"));
		isWidescreen = Boolean.valueOf(ini.get("general", "isWidescreen"));
		isMuted = Boolean.valueOf(ini.get("general", "isMuted"));
		disableAutoSetupStages = Boolean.valueOf(ini.get("general",
				"disableAutoSetupStages"));
		choiceMode = ini.get("general", "choiceMode").toUpperCase();
		destination = ini.get("general", "destination").toUpperCase();
		System.out.println("Initializing [general] settings...");
		System.out.println("  [LOGGING]:      " + logging);
		System.out.println("  [LATENCY]:      " + latency);
		System.out.println("  [WIDESCREEN]:   " + isWidescreen);
		System.out.println("  [ISMUTED]:      " + isMuted);
		System.out.println("  [SETUPSTAGES]:  " + !disableAutoSetupStages);
		System.out.println("  [CHOICEMODE]:   " + choiceMode);
		System.out.println("  [DESTINATION]:  " + destination);

		// Initialize buttons that AutoSetup will press
		keyMap.put("A", Integer.parseInt(ini.get("buttons", "a_button")));
		keyMap.put("B", Integer.parseInt(ini.get("buttons", "b_button")));
		keyMap.put("UP", Integer.parseInt(ini.get("buttons", "up")));
		keyMap.put("DOWN", Integer.parseInt(ini.get("buttons", "down")));
		keyMap.put("LEFT", Integer.parseInt(ini.get("buttons", "left")));
		keyMap.put("RIGHT", Integer.parseInt(ini.get("buttons", "right")));
		keyMap.put("START", Integer.parseInt(ini.get("buttons", "start")));

		ProfileAutoSetup.initializeTileData();
		ProfileAutoSetup.createProfileList(ini);
		ControllerAutoSetup.initializeControllerAutoSetup();

		if (!disableAutoSetupStages) {
			RulesAutoSetup.initializeStageData(ini);
		}
	}

	/**
	 * Helper method for inputting keys. Utilizes the map, adds delays for
	 * slower connections, and enables logging.
	 * 
	 * @param input
	 * @throws InterruptedException
	 */
	public static void type(String input, long wait)
			throws InterruptedException {
		if (logging) {
			System.out.println("[INPUT]: " + input);
		}

		// Press and release the key
		r.keyPress(keyMap.get(input).intValue());
		Thread.sleep(keyEventDuration);
		r.keyRelease(keyMap.get(input).intValue());

		Thread.sleep(latency);
		Thread.sleep(wait);
	}

	public static void type(String input) throws InterruptedException {
		type(input, minLatency);
	}

	public static void type(String[] inputs) throws InterruptedException {
		for (int i = 0; i < inputs.length; i++) {
			type(inputs[i]);
		}
	}

	// Navigates from the very first screen to main menu, no save file
	public static void navigateToMainMenu() throws InterruptedException {

		// Countdown!
		for (int i = 0; i < 5; i++) {
			System.out.println(5 - i + "... Click on the Project M window!!!");
			Thread.sleep(1000);
		}
		System.out.println();
		
		if (!fromMainMenu) {
			type("A", 2100);
			type("A", 1900);
			type("B", 250);
			type("A", 1100);
			type("B", 500);
			type("A", 1500);
			System.out.println("[INFO]: Navigated to main menu");
		} else {
			// Goes to splash then Main Menu. If focus is not on "FIGHT"
			type("B", 500);
			type("A", 1425);
		}
	}

	// From the main menu, sets the ratio to 16:9
	public static void setupWidescreen() throws InterruptedException {
		if (isWidescreen) {
			type(new String[] { "RIGHT", "DOWN", "A", "A", "RIGHT", "B", "B" });
			System.out.println("[INFO]: Widescreen setup completed");
		} else {
			type(new String[] { "RIGHT", "DOWN" });
		}
	}

	// Turn off sound
	private static void setupSound() throws InterruptedException {
		if (isMuted) {
			type(new String[] { "A", "DOWN", "A" });

			if (logging) {
				System.out.println("[INPUT]: " + "LEFT");
			}

			// Press and release the key
			r.keyPress(keyMap.get("LEFT").intValue());
			Thread.sleep(900);
			r.keyRelease(keyMap.get("LEFT").intValue());

			Thread.sleep(latency);
			Thread.sleep(100);

			type(new String[] { "B", "B" });

			System.out.println("[INFO]: Sound is muted");
		}
	}
}
package setup;

import java.awt.Point;

public class Stage implements Comparable<Stage> {
	Point location;
	public String name;
	public boolean isEnabled;

	public Stage(int setRow, int setCol, String setName) {
		location = new Point(setRow, setCol);
		name = setName;
	}

	public int compareTo(Stage arg0) {
		return name.compareTo(arg0.name);
	}
}

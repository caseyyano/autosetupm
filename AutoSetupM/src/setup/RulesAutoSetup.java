package setup;

import java.awt.Point;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import org.ini4j.Ini;

/**
 * Logic for setting up stages go here
 */
public class RulesAutoSetup {
	private static List<Stage> stageList = new ArrayList<Stage>();
	private static List<Stage> enabledList = new ArrayList<Stage>();
	private static boolean isApex;
	private static final String TEMPLE = "Temple";
	private static final String CUSTOM_STAGE = "CustomStagesOn";
	private static final String GREEN_HILL_ZONE = "Green Hill Zone";

	/**
	 * Initialize all the stage data. Found in data/stages.txt
	 * 
	 * @throws FileNotFoundException
	 */
	public static void initializeStageData(Ini ini)
			throws FileNotFoundException {
		System.out.println("Initializing [stage] settings...");

		isApex = Boolean.valueOf(ini.get("stage", "ApexStages"));
		if (isApex) {
			System.out.println(" Loading APEX defaults:");
			System.out.println("  BattleField\n  Dreamland");
			System.out.println("  Final Destination\n  Fountain of Dreams");
			System.out.println("  Green Hill Zone\n  Lylat Cruise");
			System.out.println("  Pokemon Stadium\n  Pokemon Stadium 2");
			System.out.println("  Skyloft\n  Skyworld");
			System.out.println("  Smashville\n  WarioWare, Inc.");
			System.out.println("  Yoshi's Island\n  Yoshi's Story\n");
			return;
		}

		Scanner scanner = new Scanner(new FileInputStream("data/stages.txt"));
		scanner.nextLine();
		while (scanner.hasNextLine()) {
			stageList.add(new Stage(scanner.nextInt(), scanner.nextInt(),
					scanner.nextLine().trim()));
		}
		scanner.close();

		boolean templeAdded = false;
		boolean customAdded = false;
		for (Stage s : stageList) {
			s.isEnabled = Boolean.valueOf(ini.get("stage", s.name));
			if (s.isEnabled) {
				if (s.name.equals(TEMPLE)) {
					templeAdded = true;
					continue;
				}
				if (!templeAdded && (s.location.x > 0 || s.location.y > 5)) {
					enabledList.add(new Stage(0, 5, TEMPLE));
					templeAdded = true;
				}
				if (s.name.equals(CUSTOM_STAGE)) {
					customAdded = true;
					continue;
				}
				if (!customAdded && s.name.equals(GREEN_HILL_ZONE)) {
					enabledList.add(new Stage(5, 5, CUSTOM_STAGE));
					customAdded = true;
				}
				enabledList.add(s);
			}
		}

		// Alphabetizing list
		List<Stage> copyForPrinting = new ArrayList<Stage>(enabledList);
		templeAdded = false;
		customAdded = false;
		for (Stage s : copyForPrinting) {
			if (s.name.equals(TEMPLE)) {
				templeAdded = true;
			}
			if (s.name.equals(CUSTOM_STAGE)) {
				customAdded = true;
			}
		}
		copyForPrinting.add(new Stage(0, 5, TEMPLE));
		copyForPrinting.add(new Stage(5, 5, CUSTOM_STAGE));

		Collections.sort(copyForPrinting);
		for (Stage s : copyForPrinting) {
			if (s.name.equals(TEMPLE) && templeAdded) {
				continue;
			}
			if (s.name.equals(CUSTOM_STAGE) && customAdded) {
				continue;
			}
			System.out.println("  " + s.name);
		}
		System.out.println();
	}

	// Turns off items
	public static void setupItems() throws InterruptedException {
		AutoSetupM.type(new String[] { "UP", "LEFT" });
		AutoSetupM.type("A", 200);
		AutoSetupM.type("RIGHT");
		AutoSetupM.type("A", 200);
		AutoSetupM.type("UP");
		AutoSetupM.type("A", 200);
		AutoSetupM.type(new String[] { "LEFT", "LEFT", "DOWN", "LEFT", "LEFT",
				"A" });
		AutoSetupM.type("B", 200);
		System.out.println("[INFO]: Items have been turned off");
	}

	/**
	 * Changes stage choice
	 * 
	 * @param choice
	 * @throws InterruptedException
	 */
	public static void setupStageChoice(String choice)
			throws InterruptedException {

		if (choice == null || !choice.equals("CHOOSE")) {
			AutoSetupM.type("UP");
			if (choice.equals("RANDOM")) {
				AutoSetupM.type("A");
			} else if (choice.equals("TURNS")) {
				AutoSetupM.type(new String[] { "A", "A" });
			} else if (choice.equals("ORDER")) {
				AutoSetupM.type(new String[] { "A", "A", "A" });
			} else if (choice.equals("LOSER")) {
				AutoSetupM.type(new String[] { "A", "A", "A", "A" });
			} else {
				System.out.println("Invalid stage choice: " + choice);
			}
			AutoSetupM.type("DOWN");
			System.out.println("[INFO]: Stage choice set to " + choice);
		}
		AutoSetupM.type("RIGHT");
	}

	/**
	 * Choose stages, woo! Will use a path finding algorithm (no joke)
	 * 
	 * @throws InterruptedException
	 */
	public static void setupStages(boolean setStages)
			throws InterruptedException {
		if (!setStages) {

			// Stage Select menu
			AutoSetupM.type(new String[] { "A", "UP" });
			AutoSetupM.type("A", 200);

			// Set all to off
			AutoSetupM.type(new String[] { "A", "RIGHT", "A" });

			if (isApex) {
				setupApexStages();
			} else {
				setupCustomStages();
			}

			System.out.println("[INFO]: Stages configured");
			AutoSetupM.type(new String[] { "B", "B", });
		}
		// Exit stage setup
		AutoSetupM.type(new String[] { "B", "B" });
	}

	/**
	 * Sets stages based on the enabledList created from settings.ini
	 * 
	 * @throws InterruptedException
	 */
	public static void setupCustomStages() throws InterruptedException {
		// Go to row 0 column 0, our starting point
		AutoSetupM.type(new String[] { "LEFT", "DOWN" });
		Point current = new Point(0, 0);

		for (Stage s : enabledList) {
			current = stageSelectHelper(current, s);
		}
	}

	/**
	 * Helper method to navigate from stage to stage
	 * 
	 * @param row
	 * @param col
	 * @param targetStage
	 * @throws InterruptedException
	 */
	public static Point stageSelectHelper(Point current, Stage targetStage)
			throws InterruptedException {
		Point target = targetStage.location;

		if (targetStage.name.equals(GREEN_HILL_ZONE)) {
			if (current.x == 5 && current.y == 5) {
				AutoSetupM.type(new String[] { "RIGHT", "DOWN", "A" });
				return new Point(6, 0);
			}
			target = new Point(5, 0);
		}
		System.out.println(targetStage.name);

		// Navigate columns
		while (current.y != target.y) {
			if (target.y > current.y) {
				current.y++;
				AutoSetupM.type("RIGHT");
			} else {
				current.y--;
				AutoSetupM.type("LEFT");
			}
		}

		// Navigate rows
		while (current.x != target.x) {
			if (target.x > current.x) {
				current.x++;
				AutoSetupM.type("DOWN");
			}
		}

		// This handles the case where custom stage is on (why?)
		if (targetStage.name.equals(GREEN_HILL_ZONE)) {
			AutoSetupM.type("DOWN");
		}

		AutoSetupM.type("A");
		return current;
	}

	/**
	 * Sets stages from the APEX stage list. Takes an optimized route
	 * 
	 * @throws InterruptedException
	 */
	public static void setupApexStages() throws InterruptedException {
		// Yoshi's Story (melee)
		AutoSetupM.type(new String[] { "DOWN", "RIGHT", "A" });
		// Turn off Temple
		AutoSetupM.type(new String[] { "LEFT", "A" });
		// Battlefield
		AutoSetupM.type(new String[] { "RIGHT", "RIGHT", "A" });
		// Final Destination
		AutoSetupM.type(new String[] { "RIGHT", "A" });
		// Yoshi's Island
		AutoSetupM.type(new String[] { "DOWN", "DOWN", "A" });
		// Lylat Cruise
		AutoSetupM.type(new String[] { "RIGHT", "RIGHT", "A" });
		// WarioWare, Inc.
		AutoSetupM.type(new String[] { "DOWN", "A" });
		// Skyworld
		AutoSetupM.type(new String[] { "DOWN", "A" });
		// Skyloft
		AutoSetupM.type(new String[] { "DOWN", "A" });
		// Turn off Custom Stages
		AutoSetupM.type(new String[] { "RIGHT", "RIGHT", "A" });
		// Fountain of Dreams
		AutoSetupM.type(new String[] { "RIGHT", "A" });
		// Green Hill Zone
		AutoSetupM.type(new String[] { "DOWN", "A" });
		// Smashville
		AutoSetupM.type(new String[] { "UP", "UP", "A" });
		// Pokemon Stadium (melee)
		AutoSetupM.type(new String[] { "LEFT", "A" });
		// Dreamland (melee)
		AutoSetupM.type(new String[] { "UP", "UP", "LEFT", "A" });
		// Pokemon Stadium 2
		AutoSetupM.type(new String[] { "LEFT", "A" });
	}
}

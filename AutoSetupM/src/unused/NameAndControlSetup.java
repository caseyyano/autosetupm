package unused;

import java.io.File;
import java.util.prefs.Preferences;

import org.ini4j.Ini;
import org.ini4j.IniPreferences;

/**
 * Runs NameSetup and ControlSetup back-to-back (not yet implemented)
 */
public class NameAndControlSetup {

	public static final String FILENAME = "config/profiles.ini";

	public static void main(String[] args) throws Exception {
		Preferences prefs = new IniPreferences(new Ini(new File(FILENAME)));

		String[] children = prefs.childrenNames();

		for (int i = 0; i < children.length; i++) {
			String name = children[i];
			Preferences child = prefs.node(name);
			NameSetup.SetName(name);
		}

		System.out.println("grumpy/homePage: "
				+ prefs.node("grumpy").get("homePage", null));
	}

}

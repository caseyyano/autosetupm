package unused;

public class Letter {
	String letter;
	int page;
	int column;
	int row;
	int clicks;

	public Letter(String letter, int page, int column, int row, int clicks)
	{
		this.letter = letter;
		this.page = page;
		this.column = column;
		this.row = row;
		this.clicks = clicks;
	}

}
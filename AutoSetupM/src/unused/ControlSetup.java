package unused;

/**
 * Unfinished code to setup the controller
 */
public class ControlSetup {

	// Controls Keys
	private static final String LBUMPER = "Lbumper";
	private static final String RBUMPER = "Rbumper";
	private static final String TAPJUMP = "Tap Jump";
	private static final String Z = "Z";
	private static final String Y = "Y";
	private static final String X = "X";
	private static final String DPADUP = "DpadUp";
	private static final String A = "A";
	private static final String DPADSIDE = "DpadSide";
	private static final String B = "B";
	private static final String DPADDOWN = "DpadDown";
	private static final String CSTICK = "CStick";
}

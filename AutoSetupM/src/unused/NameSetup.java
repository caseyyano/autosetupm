package unused;

import java.util.HashMap;

/**
 * Unlike ProfileAutoSetup, this one utilizes all pages like JP and symbols.
 * However, this one is not yet implemented...
 */
public class NameSetup {

	private static HashMap<String, Letter> letters;

	public static void initialize() {
		letters.put("_", new Letter("_", 0, 0, 0, 1));
		letters.put("@", new Letter("@", 0, 0, 0, 2));
		letters.put("(", new Letter("(", 0, 0, 0, 3));
		letters.put(")", new Letter(")", 0, 0, 0, 4));
		letters.put("^", new Letter("^", 0, 0, 0, 5));
		letters.put(":", new Letter(":", 0, 0, 0, 6));
		letters.put(";", new Letter(";", 0, 0, 0, 7));
		letters.put("A", new Letter("A", 0, 0, 1, 1));
		letters.put("B", new Letter("B", 0, 0, 1, 2));
		letters.put("C", new Letter("C", 0, 0, 1, 3));
		letters.put("D", new Letter("D", 0, 0, 2, 1));
		letters.put("E", new Letter("E", 0, 0, 2, 2));
		letters.put("F", new Letter("F", 0, 0, 2, 3));
		letters.put("G", new Letter("G", 0, 1, 0, 1));
		letters.put("H", new Letter("H", 0, 1, 0, 2));
		letters.put("I", new Letter("I", 0, 1, 0, 3));
		letters.put("J", new Letter("J", 0, 1, 1, 1));
		letters.put("K", new Letter("K", 0, 1, 1, 2));
		letters.put("L", new Letter("L", 0, 1, 1, 3));
		letters.put("M", new Letter("M", 0, 1, 2, 1));
		letters.put("N", new Letter("N", 0, 1, 2, 2));
		letters.put("O", new Letter("O", 0, 1, 2, 3));
		letters.put("P", new Letter("P", 0, 2, 0, 1));
		letters.put("Q", new Letter("Q", 0, 2, 0, 2));
		letters.put("R", new Letter("R", 0, 2, 0, 3));
		letters.put("S", new Letter("S", 0, 2, 0, 4));
		letters.put("T", new Letter("T", 0, 2, 1, 1));
		letters.put("U", new Letter("U", 0, 2, 1, 2));
		letters.put("V", new Letter("V", 0, 2, 1, 3));
		letters.put("W", new Letter("W", 0, 2, 2, 1));
		letters.put("X", new Letter("X", 0, 2, 2, 2));
		letters.put("Y", new Letter("Y", 0, 2, 2, 3));
		letters.put("Z", new Letter("Z", 0, 2, 2, 4));
		letters.put("!", new Letter("!", 0, 3, 0, 1));
		letters.put("?", new Letter("?", 0, 3, 0, 2));
		letters.put("&", new Letter("&", 0, 3, 0, 3));
		letters.put("%", new Letter("%", 0, 3, 0, 4));
		letters.put("*", new Letter("*", 0, 3, 1, 1));
		letters.put(",", new Letter(",", 0, 3, 1, 2));
		letters.put("0", new Letter("0", 0, 3, 1, 3));
		letters.put("/", new Letter("/", 0, 3, 1, 4));
		letters.put("~", new Letter("~", 0, 3, 1, 5));
		letters.put("Swap", new Letter("Swap", 0, 3, 2, 1));
		letters.put("À", new Letter("À", 1, 0, 0, 1));
		letters.put("Ç", new Letter("Ç", 1, 0, 1, 1));
		letters.put("È", new Letter("È", 1, 0, 2, 1));
		letters.put("Ì", new Letter("Ì", 1, 1, 0, 1));
		letters.put("Ò", new Letter("Ò", 1, 1, 1, 1));
		letters.put("Ñ", new Letter("Ñ", 1, 1, 2, 1));
		letters.put("ß", new Letter("ß", 1, 1, 2, 2));
		letters.put("Ù", new Letter("Ù", 1, 2, 0, 1));
		letters.put("¡", new Letter("¡", 1, 2, 1, 1));
		letters.put("¿", new Letter("¿", 1, 2, 1, 2));
		letters.put("£", new Letter("£", 1, 2, 1, 3));
		letters.put("€", new Letter("€", 1, 2, 1, 4));
		letters.put("_", new Letter("_", 1, 2, 2, 1));
		letters.put(",", new Letter(",", 1, 2, 2, 2));
		letters.put("", new Letter("", 1, 2, 2, 3));
		letters.put("°", new Letter("°", 1, 2, 2, 4));
		letters.put(":", new Letter(":", 1, 3, 0, 1));
		letters.put(";", new Letter(";", 1, 3, 0, 2));
		letters.put("(", new Letter("(", 1, 3, 0, 3));
		letters.put(")", new Letter(")", 1, 3, 0, 4));
		letters.put("↑", new Letter("↑", 1, 3, 1, 1));
		letters.put("↓", new Letter("↓", 1, 3, 1, 2));
		letters.put("←", new Letter("←", 1, 3, 1, 3));
		letters.put("→", new Letter("→", 1, 3, 1, 4));
		letters.put("Swap", new Letter("Swap", 1, 3, 2, 1));
		letters.put("a", new Letter("a", 2, 0, 0, 1));
		letters.put("ka", new Letter("ka", 2, 0, 1, 1));
		letters.put("sa", new Letter("sa", 2, 0, 2, 1));
		letters.put("ta", new Letter("ta", 2, 1, 0, 1));
		letters.put("na", new Letter("na", 2, 1, 1, 1));
		letters.put("ha", new Letter("ha", 2, 1, 2, 1));
		letters.put("ma", new Letter("ma", 2, 2, 0, 1));
		letters.put("ya", new Letter("ya", 2, 2, 1, 1));
		letters.put("ra", new Letter("ra", 2, 2, 2, 1));
		letters.put("tenten", new Letter("tenten", 2, 3, 0, 1));
		letters.put("maru", new Letter("maru", 2, 3, 0, 2));
		letters.put("wa", new Letter("wa", 2, 3, 1, 1));
		letters.put("o", new Letter("o", 2, 3, 1, 2));
		letters.put("n", new Letter("n", 2, 3, 1, 3));
		letters.put("-0", new Letter("-0", 2, 3, 1, 4));
		letters.put("~", new Letter("~", 2, 3, 1, 5));
		letters.put("Swap", new Letter("Swap", 2, 3, 2, 1));
		letters.put("a", new Letter("a", 3, 0, 0, 1));
		letters.put("ka", new Letter("ka", 3, 0, 1, 1));
		letters.put("sa", new Letter("sa", 3, 0, 2, 1));
		letters.put("ta", new Letter("ta", 3, 1, 0, 1));
		letters.put("na", new Letter("na", 3, 1, 1, 1));
		letters.put("ha", new Letter("ha", 3, 1, 2, 1));
		letters.put("ma", new Letter("ma", 3, 2, 0, 1));
		letters.put("ya", new Letter("ya", 3, 2, 1, 1));
		letters.put("ra", new Letter("ra", 3, 2, 2, 1));
		letters.put("tenten", new Letter("tenten", 3, 3, 0, 1));
		letters.put("maru", new Letter("maru", 3, 3, 0, 2));
		letters.put("wa", new Letter("wa", 3, 3, 1, 1));
		letters.put("o", new Letter("o", 3, 3, 1, 2));
		letters.put("n", new Letter("n", 3, 3, 1, 3));
		letters.put("-0", new Letter("-0", 3, 3, 1, 4));
		letters.put("~", new Letter("~", 3, 3, 1, 5));
		letters.put("Swap", new Letter("Swap", 3, 3, 2, 1));
		letters.put("1", new Letter("1", 4, 0, 0, 1));
		letters.put("2", new Letter("2", 4, 0, 1, 1));
		letters.put("3", new Letter("3", 4, 0, 2, 1));
		letters.put("4", new Letter("4", 4, 1, 0, 1));
		letters.put("5", new Letter("5", 4, 1, 1, 1));
		letters.put("6", new Letter("6", 4, 1, 2, 1));
		letters.put("7", new Letter("7", 4, 2, 0, 1));
		letters.put("8", new Letter("8", 4, 2, 1, 1));
		letters.put("9", new Letter("9", 4, 2, 2, 1));
		letters.put("-0", new Letter("-0", 4, 3, 0, 1));
		letters.put("0", new Letter("0", 4, 3, 0, 2));
		letters.put("*", new Letter("*", 4, 3, 0, 3));
		letters.put("=", new Letter("=", 4, 3, 0, 4));
		letters.put("0", new Letter("0", 4, 3, 1, 5));
		letters.put("Swap", new Letter("Swap", 4, 3, 2, 1));
	}

	/**
	 * Logic to actually parse and type out the name goes here
	 * 
	 * @param name
	 */
	public static void SetName(String name) {
		if (name.length() > 5) {
			System.out.println("Name: " + name
					+ " is too long! Using first 5 characters...");
		} else {
			System.out.println(name);
		}
		// TODO: All the logic in the description goes here
	}
}
